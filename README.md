# Vagrant & Agnular CLI

📺 YouTube: https://www.youtube.com/watch?v=AivZhY8DdY8

Tested on Vagrant 2.0.3 (2018). The script will be updated in the future.

---

- 1
  - An initial setup with Angular & Vagrant
- 2
  - A modification to the previous configuration using a non-blocking command `nohup` to run Angular
- 3
  - An example of `run: "always"` parameter which allows to run a particular command on every `vagrant up` execution

## Angular

Angular is a development platform, built on TypeScript. As a platform, Angular includes:
- A component-based framework for building scalable web applications
- A collection of well-integrated libraries that cover a wide variety of features, including routing, forms management, client-server communication, and more
- A suite of developer tools to help you develop, build, test, and update your code

Ref: https://angular.io/guide/what-is-angular

## Vagrant

Vagrant is a tool for building and managing virtual machine environments in a single workflow. With an easy-to-use workflow and focus on automation, Vagrant lowers development environment setup time, increases production parity, and makes the "works on my machine" excuse a relic of the past.

Vagrant provides easy to configure, reproducible, and portable work environments built on top of industry-standard technology and controlled by a single consistent workflow to help maximize the productivity and flexibility of you and your team.

To achieve its magic, Vagrant stands on the shoulders of giants. Machines are provisioned on top of VirtualBox, VMware, AWS, or any other provider. Then, industry-standard provisioning tools such as shell scripts, Chef, or Puppet can automatically install and configure software on the virtual machine.

Ref: https://developer.hashicorp.com/vagrant/intro
